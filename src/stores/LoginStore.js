import { defineStore } from "pinia";
import { apiLogin, apiRegister, getCaptchaUrl } from "../service/User";

export const useLoginStore = defineStore("login", {
  state: () => ({
    HeaderTitle: "",
    captchaUrl: "",
    captchaCheck: "",
  }),
  actions: {
    //切换标题
    changeTitle(title) {
      this.HeaderTitle = title;
    },
    //刷新验证码
    async updateCaptchaUrl() {
      try {
        let params = {
          len: 5,
          type: 0,
          app_id: "tkwattpvlwjjolwl",
          app_secret: "d2FueUFZOTlUcncybEtYSk0wZEE5dz09",
        };
        const res = await getCaptchaUrl(params);
        this.captchaUrl = res.data.data.verifyCodeImgUrl;
        this.captchaCheck = res.data.data.verifyCode;
      } catch (err) {
        console.log(err);
      }
    },
    //登录
    async login(params) {
      try {
        const res = await apiLogin(params);
        return res;
      } catch (err) {
        console.log(err);
      }
    },
    //注册
    async register(params) {
      try {
        const res = await apiRegister(params);
        return res;
      } catch (err) {
        console.log(err);
      }
    },
  },
});
