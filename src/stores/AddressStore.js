import { defineStore } from "pinia";
import { apiGetUserAddress } from "../service/User";

export const useAddressStore = defineStore("addressStore", {
  state: () => ({
    addressData: [],
  }),
  actions: {
    async getAddress() {
      try {
        if (this.addressData.length != 0) {
          return;
        }
        const res = await apiGetUserAddress();
        // console.log(res);
        this.addressData = res.data.data;
      } catch (err) {
        console.log(err);
      }
    },

    updateAddress(id, address) {
      let index = -1;
      for (let i = 0; i < this.addressData.length; i++) {
        if (id == this.addressData[i].addressId) {
          index = i;
        }
      }
      this.addressData[index] = address;
    },
  },
});
