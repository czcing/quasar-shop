import { defineStore } from "pinia";
import { apiGetUserAddressEditById } from "../service/User";
export const useAddressEditStore = defineStore("addressEditStore", {
  state: () => ({
    address: {},
  }),
  actions: {
    async getAddressById(id) {
      console.log("id=" + id);
      try {
        const res = await apiGetUserAddressEditById(id);
        this.address = res.data.data;
      } catch (err) {
        console.log(err);
      }
    },

    clearAddress() {
      this.address = {};
    },
  },
});
