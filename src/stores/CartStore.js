import { defineStore } from "pinia";
import { apiGetShopCart } from "../service/cart";

export const useCartStore = defineStore("cart", {
  state: () => ({
    cartList: [],
  }),
  getters: {
    isAllSelected: (state) => {
      let flag = false;
      for (let i = 0; i < state.cartList.length; i++) {
        if (!state.cartList[i].isSelect) {
          flag = false;
        }
        console.log(flag);
      }
      return flag;
    },
  },
  actions: {
    async getShopCart() {
      const res = await apiGetShopCart();
      if ((res.resultCode = "200")) {
        this.cartList = res.data.data;
      }
      return res.data.resultCode + ":" + res.data.message;
    },

    toggleAllSelect() {
      for (item in cartList) {
        item.isSelect = this.isAllSelected;
      }
      console.log(this.isAllSelected);
    },
  },
});
