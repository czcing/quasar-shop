import { defineStore } from "pinia";
import { apiGetCategoryList, apiGetHomeInfo } from "../service/home";

export const useHomeStore = defineStore("Home", {
  state: () => ({
    categoryList1: [],
    categoryList2: [],
    carousels: [],
    hotGoodses: [],
    newGoodses: [],
    recommendGoodses: [],
  }),

  actions: {
    async getHomeCategory() {
      try {
        const res = await apiGetCategoryList();
        this.categoryList1 = res.data.data[0].categoryList1;
        this.categoryList2 = res.data.data[0].categoryList2;
        return;
      } catch (err) {
        console.log(err);
      }
    },
    async getHomeInfo() {
      try {
        const res = await apiGetHomeInfo();
        this.carousels = res.data.data.carousels;
        this.hotGoodses = res.data.data.hotGoodses;
        this.newGoodses = res.data.data.newGoodses;
        this.recommendGoodses = res.data.data.recommendGoodses;
        return;
      } catch (err) {
        console.log(err);
      }
    },
  },
});
