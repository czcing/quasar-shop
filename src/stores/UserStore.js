import { defineStore } from "pinia";
import { apiGetUserInfo } from "../service/User";

export const useUserStore = defineStore("userStore", {
  state: () => ({
    userInfo: {},
  }),

  actions: {
    async getUserInfo() {
      try {
        const res = await apiGetUserInfo();
        this.userInfo = res.data.data;
      } catch (err) {
        console.log(err);
      }
    },
  },
});
