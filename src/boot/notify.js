import { Notify } from "quasar";

Notify.create("Danger, Will Robinson! Danger!");
// 或使用配置对象：
Notify.create({
  message: "Danger, Will Robinson! Danger!",
});
