import { boot } from "quasar/wrappers";
import axios from "axios";
import { LocalStorage, Notify } from "quasar";
import { routerInstance } from "../router/index";

// Be careful when using SSR for cross-request state pollution
// due to creating a Singleton instance here;
// If any client changes this (global) instance, it might be a
// good idea to move this instance creation inside of the
// "export default () => {}" function below (which runs individually
// for each client)
const api = axios.create({ baseURL: "http://localhost:3000" });
// baseURL: "https://www.mxnzp.com"
// const api = axios.create({ baseURL: 'https://api.example.com' })
const captchaApi = axios.create({
  baseURL: "https://www.mxnzp.com",
});

const shopApi = axios.create({
  baseURL: "http://localhost:3001",
});

api.defaults.headers.post["Content-Type"] = "application/json";
api.defaults.headers.common["Authorization"] = `Bearer ${LocalStorage.getItem(
  "access_token"
)}`;

api.interceptors.response.use((res) => {
  if (typeof res.data !== "object") {
    Notify.create("服务端异常！");
    return Promise.reject(res);
  }
  if (res.status != 200) {
    if (res.data.message) Notify.create(res.data.message);
    if (res.status == 416) {
      routerInstance.push({ path: "/login" });
    }
    if (res.data.data && window.location.hash == "#/login") {
      LocalStorage.setItem("access_token", res.data.data);
      api.defaults.headers.common["Authorization"] = res.data.data;
    }
    return Promise.reject(res.data);
  }

  return res;
});

shopApi.interceptors.response.use((res) => {
  if (typeof res.data !== "object") {
    Notify.create("服务端异常！");
    return Promise.reject(res);
  }
  if (res.status != 200) {
    if (res.data.message) Notify.create(res.data.message);
    if (res.status == 416) {
      routerInstance.push({ path: "/login" });
    }
    if (res.data.data && window.location.hash == "#/login") {
      LocalStorage.setItem("access_token", res.data.data);
      api.defaults.headers.common["Authorization"] = res.data.data;
    }
    return Promise.reject(res.data);
  }

  return res;
});

captchaApi.interceptors.response.use((res) => {
  if (typeof res.data !== "object") {
    Notify.create("服务端异常！");
    return Promise.reject(res);
  }
  if (res.status != 200) {
    if (res.data.message) Notify.create(res.data.message);
    if (res.status == 416) {
      routerInstance.push({ path: "/login" });
    }
    if (res.data.data && window.location.hash == "#/login") {
      LocalStorage.setItem("access_token", res.data.data);
      api.defaults.headers.common["Authorization"] = res.data.data;
    }
    return Promise.reject(res.data);
  }

  return res;
});

export default boot(({ app }) => {
  // for use inside Vue files (Options API) through this.$axios and this.$api

  app.config.globalProperties.$axios = axios;
  // ^ ^ ^ this will allow you to use this.$axios (for Vue Options API form)
  //       so you won't necessarily have to import axios in each vue file

  app.config.globalProperties.$api = { api, captchaApi, homeApi: shopApi };
  // app.config.globalProperties.$api = captchaApi;
  // ^ ^ ^ this will allow you to use this.$api (for Vue Options API form)
  //       so you can easily perform requests against your app's API
});

export { api, captchaApi, shopApi };
