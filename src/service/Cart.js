import { shopApi } from "../boot/axios";

export function apiGetShopCart() {
  return shopApi.get("/shop-cart");
}
