import { shopApi } from "../boot/axios";

export function apiGetCategoryList() {
  return shopApi.get("/home/category");
}

export function apiGetHomeInfo() {
  return shopApi.get("index-infos");
}
