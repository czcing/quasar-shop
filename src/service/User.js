import { api, captchaApi, shopApi } from "../boot/axios";

export function apiLogin(params) {
  return api.post("/login", params);
}

export function apiRegister(params) {
  return api.post("/register", params);
}

export function getCaptchaUrl(params) {
  return captchaApi.get("/api/verifycode/code", { params });
}

export function apiGetUserInfo() {
  return shopApi.get("/user/info");
}

export function apiGetUserAddress() {
  return shopApi.get("/address");
}

export function apiGetUserAddressEditById(id) {
  return shopApi.get(`/address/edit/${id}`);
}
