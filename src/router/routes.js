const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      // {
      //   path: "",
      //   component: () => import("pages/IndexPage.vue"),
      // },
      {
        path: "/",
        component: () => import("pages/HomePage.vue"),
      },
      {
        path: "/category",
        component: () => import("pages/CategoryPage.vue"),
      },
      {
        path: "/cart",
        component: () => import("pages/CartPage.vue"),
      },
      {
        path: "/user",
        component: () => import("pages/UserPage.vue"),
      },
      {
        path: "/product/:id",
        component: () => import("pages/ProductDetailPage.vue"),
      },
    ],
  },
  {
    path: "/address",
    component: () => import("pages/AddressPage.vue"),
  },
  {
    path: "/address/edit/:id",
    component: () => import("pages/AddressEditPage.vue"),
  },
  {
    path: "/",
    component: () => import("layouts/LoginLayout.vue"),
    children: [
      {
        path: "/login",
        component: () => import("pages/LoginPage.vue"),
      },
      {
        path: "/register",
        component: () => import("pages/RegisterPage.vue"),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
